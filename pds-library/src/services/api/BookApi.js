import BaseCrudEntity from "./BaseCrudEntity";

export default class extends BaseCrudEntity {
  constructor(request) {
    super("books", request);
  }

  create(obj) {
    return this._request("POST", this._endpoint, { ...obj, author: obj.author.id });
  }

  edit(id, obj) {
    return this._request("PUT", this._endpoint + `/${id}`, { ...obj, author: obj.author.id });
  }
}
