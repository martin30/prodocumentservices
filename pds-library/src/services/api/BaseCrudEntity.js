export default class {
  _request;
  _endpoint;

  constructor(endpoint, request) {
    this._endpoint = endpoint;
    this._request = request;
  }

  getAll() {
    return this._request("GET", this._endpoint);
  }

  getOne(id) {
    return this._request("GET", this._endpoint + `/${id}`);
  }

  create(obj) {
    return this._request("POST", this._endpoint, obj);
  }

  edit(id, obj) {
    return this._request("PUT", this._endpoint + `/${id}`, obj);
  }

  remove(id) {
    return this._request("DELETE", this._endpoint + `/${id}`);
  }
}
