import BaseCrudEntity from "./BaseCrudEntity";

export default class extends BaseCrudEntity {
  constructor(request) {
    super("authors", request);
  }
}
