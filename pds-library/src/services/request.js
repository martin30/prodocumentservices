import axios from "axios";

export default config => (method, endpoint, data) => {
  return axios({
    method,
    url: config.API_URL + "/" + endpoint,
    data
  }).then(res => res.data);
};
