import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import pdsCrud from "./modules/pds-crud";
import config from "./config";
import requestBase from "./services/request";
import BookApi from "./services/api/BookApi";
import AuthorApi from "./services/api/AuthorApi";

Vue.config.productionTip = false;

let request = requestBase(config);

Vue.use(BootstrapVue);
Vue.use(pdsCrud, {
  request
});

let api = {
  books: new BookApi(request),
  authors: new AuthorApi(request)
};

Vue.prototype.$api = api;

new Vue({
  router: router(api),
  store,
  render: h => h(App)
}).$mount("#app");
