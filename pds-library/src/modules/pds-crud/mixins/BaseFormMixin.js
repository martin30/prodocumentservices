export default {
  props: {
    value: {
      type: Object,
      default: () => ({})
    }
  },
  created() {
    this.form = { ...this.value };
  },
  watch: {
    value: {
      deep: true,
      handler(newVal) {
        this.form = newVal;
      }
    },
    form: {
      deep: true,
      handler(newVal) {
        this.$emit("input", this.form);
      }
    }
  }
};
