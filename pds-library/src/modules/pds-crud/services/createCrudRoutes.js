import CreateEntityForm from "../components/CreateEntityForm";
import EditEntityForm from "../components/EditEntityForm";

export default function(baseRoute) {
  let entityName = baseRoute.name.split(".").slice(-1)[0];

  return [
    {
      path: entityName + "/create",
      name: baseRoute.name + ".create",
      component: CreateEntityForm,
      meta: {
        ...baseRoute.meta,
        redirect: baseRoute.name
      }
    },
    {
      path: entityName + "/edit/:id",
      name: baseRoute.name + ".edit",
      component: EditEntityForm,
      meta: {
        ...baseRoute.meta,
        redirect: baseRoute.name
      }
    }
  ];
}
