import PdsCrudTable from "./components/PdsCrudTable.vue";

export default function(Vue, { request }) {
  Vue.component("PdsCrudTable", PdsCrudTable);
}
