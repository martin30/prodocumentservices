import Vue from "vue";
import VueRouter from "vue-router";
import Admin from "../views/Admin";
import AdminBooks from "../views/AdminBooks";
import AdminAuthors from "../views/AdminAuthors";
import createCrudRoutes from "../modules/pds-crud/services/createCrudRoutes";
import AuthorForm from "../components/forms/AuthorForm";
import BookForm from "../components/forms/BookForm";

Vue.use(VueRouter);

export default api => {
  const routes = [
    {
      path: "/admin",
      name: "admin",
      redirect: { name: "admin.books" },
      component: Admin,
      children: [
        {
          path: "authors",
          name: "admin.authors",
          component: AdminAuthors
        },
        ...createCrudRoutes({
          name: "admin.authors",
          meta: {
            formComponent: AuthorForm,
            api: api.authors
          }
        }),
        {
          path: "books",
          name: "admin.books",
          component: AdminBooks
        },
        ...createCrudRoutes({
          name: "admin.books",
          meta: {
            formComponent: BookForm,
            api: api.books
          }
        })
      ]
    }
  ];

  const router = new VueRouter({
    linkActiveClass: "active",
    routes
  });

  return router;
};
