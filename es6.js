const timeout = () => {
  return new Promise(((resolve, reject) => {
    setTimeout(() => resolve(), 500);
  }))
}

console.time()


timeout()
  .then(() => console.timeEnd())

const timeoutOld = (cb) => {
  setTimeout(() => cb(), 500);
}

console.time()

timeout().then(() => console.timeEnd())

timeoutOld(() => console.log("timeout ended"));

// callback style
const checkToken = (cb) => {
};
const getUserInfo = (cb) => {
};

checkToken(() => {
  getUserInfo(() => {

  })
})

// promise style
const checkToken2 = (cb) => new Promise((resolve, reject) => {
});
const getUserInfo2 = (cb) => new Promise((resolve, reject) => {
});

checkToken2()
  .then((token) => {
    console.log(token);
    return getUserInfo2()
  })
  .then((userInfo) => {

  })

// promise resolve / reject
// promise style
const checkToken2 = (cb) => new Promise((resolve, reject) => {
  resolve()
});

checkToken2()
  .then((token) => {
    return Promise.reject("nectr");
  })
  .then((userInfo) => {
    console.log("resolved")
  }, (userInfo) => {
    console.log("rejected")
  })

checkToken2()
  .then((token) => {
    return Promise.reject("nectr");
  })
  .then((userInfo) => {
    console.log("resolved")
  })
  .then((userInfo) => timeout(500))
  .catch(() => {
    console.log("rejected")
  })


const getCountries = (cb) => new Promise((resolve, reject) => {
});
const getUserInfo2 = (cb) => new Promise((resolve, reject) => {
});

Promise
  .all([getCountries(), getUserInfo2()])
  .then(([countries, userInfo]) => {

  })
  .finally(() => {
    // loading = false
  })

class Furniture {
  get prop1() {
    return "test"
  }

  constructor() {

  }
}


class Table extends Furniture {
  constructor() {
    super();

    this.prop1;
  }
}